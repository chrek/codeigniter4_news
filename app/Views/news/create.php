<div class="form">
  <?= \Config\Services::validation()->listErrors(); ?>
  <form action="/news/create" method="post">
    <fieldset>
      <legend><?= esc($title); ?></legend>
      <?= csrf_field() ?>
      <label for="title">Title</label>
      <input type="text" name="title"><br><br>
    
      <label for="body">Text</label>
      <textarea name="body" id=""></textarea><br><br>
    </fieldset>
  
    <input type="submit" name="submit" value="Create news item">
  </form>
</div>