<?php namespace App\Controllers;

use CodeIgniter\CodeIgniter;

use CodeIgniter\Controller;

class Pages extends Controller
{
	public function index()
	{
		return view('welcome_message');
	}
	public function view($page = 'home')
	{
		echo APPPATH.'/Views/pages/'.$page.'.php';
		if (!is_file(APPPATH.'/Views/pages/'.$page.'.php')) {
			// Sorry, No such page
			throw new \CodeIgniter\Exceptions\PageNotFoundException($page);			
		}		

		$data['title'] = ucfirst($page);

		echo view('templates/header', $data); // displays the header
		echo view('pages/'.$page, $data);
		echo view('templates/footer', $data);
	}
	//--------------------------------------------------------------------

}
